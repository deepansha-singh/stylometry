# Stylometry


**GROUP 40** 

**Members** 
- Deepansha Singh
- Palak Goel 

**OBJECTIVE**

- Replicating the below mentioned stylometric case study on the Federalist Papers.

    [Introduction to Stylometry with Python](https://programminghistorian.org/en/lessons/introduction-to-stylometry-with-python)

- To get a better hands-on the primitive aspects of stylometry, whisking up our own parameters, and then formulating a corpus for 4 authors right from scratch, namely:
    - Honoré de Balzac
    - Charles Dickens
    - Anton Pavlovich Chekhov
    - Mary Wollstonecraft Shelly

**ABOUT THE CASE STUDY**

**Federalist Papers**

The Federalist Papers (also known simply as the Federalist) are a collection of 85 seminal political theory articles published between October 1787 and May 1788. These papers, written as the debate over the ratification of the Constitution of the United States was raging, presented the case for the system of government that the U.S. ultimately adopted and under which it lives to this day. As such, the Federalist is sometimes described as America’s greatest and most lasting contribution to the field of political philosophy.

Three of the Early Republic’s most prominent men wrote the papers:

- Alexander Hamilton, first Secretary of the Treasury of the United States.
- James Madison, fourth President of the United States and the man sometimes called the “Father of the Constitution” for his key role at the 1787 Constitutional Convention.
- John Jay, first Chief Justice of the United States, second governor of the State of New York, and diplomat.

However, who wrote which of the papers was a matter of open debate for 150 years, and the co-authors’ behavior is to blame for the mystery.

**Aim**

To determine authorship of the 12 disputed Federalist Papers between Alexander Hamilton and James Madison.

**Techniques Used**

- Mendenhall's Characteristics Curves of Composition
- Kilgariff's Chi-Squared Method
- John Burrows' Delta Method


**TECH-STACK** 

- Python
- Natural Language Toolkit (nltk)

**REFERENCES**

- [A Mathematical Explanation of Burrows’s Delta by Illinois Institute of Technology, Chicago](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwiYs82mrMDrAhWLzTgGHfQHD9YQFjAAegQIAxAB&url=https%3A%2F%2Fpdfs.semanticscholar.org%2F52b3%2F85e2d0b098a2dadd279cf1787ea0291a7c95.pdf&usg=AOvVaw0NjhgMIhzknc4NJdxfECZ4)
- [Ideas for Basic Parameters](https://www.cs.nmt.edu/~ramyaa/publications/ml_techniques_Stylometry.pdf)